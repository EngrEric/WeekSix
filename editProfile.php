<?php

session_start();
if(!isset($_SESSION["first_name"])){
    header("Location: signin.php");
    exit();
}

include "db.php"; 

if(!isset($_SESSION['id']) || !isset($_POST['user_address'])){
    if(!isset($_SESSION['id'])){
       
        try{
            
            $query = "SELECT id from users WHERE email = :email";
            $stmt = $conn->prepare($query);

            $stmt->bindParam(':email',$_SESSION['email']);
            $stmt->execute();

            $num = $stmt->rowCount();

            if($num>0){
                $row = $stmt->fetch();
                $_SESSION['id'] = $row['id'];
                header("Location: profileForm.php");
                exit();
            }
        } 
        //SHOW ERROE
            catch(PDOException $exception)
            {
            die("Error : " . $exception->getMessage());
            }

    }
 }else{
    try{
        $query = "SELECT * FROM user_profile WHERE user_id = :id";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(':id', $_SESSION['id']);

        $stmt->execute();

        $num = $stmt->rowCount();

        if($num == 0){
            $query = "INSERT INTO user_profile SET user_id = :id, user_address = :user_address, created_at = :created_at";

            $stmt = $conn->prepare($query);

            $id = $_SESSION['id'];
            $user_address = $_POST['user_address'];
            $created_at = date('Y-m-d H:i:s');

            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':user_address', $user_address);
            $stmt->bindParam(':created_at', $created_at);


                if($stmt->execute()){
                    header('Location: dashboard.php');
                    exit();
                }else{
                    echo "<div class='alert alert-danger'>Error:</div>";
                }
            
            }else{
                $query = "UPDATE user_profile SET user_address = :user_address WHERE user_id = :id";
                $stmt = $conn->prepare($query);

                $id = $_SESSION['id'];
                $user_address = $_POST['user_address'];

                $stmt->bindParam(':id', $id);
                $stmt->bindParam(':user_address', $user_address);

                if($stmt->execute()){
                    header('Location: dashboard.php');
                    exit();
                }else{
                    echo "<div class='alert alert-danger'>Unable to save record</div>";
                }
               
            }
        }
            //SHOW ERROE
        catch(PDOException $exception)
        {
        die("Error : " . $exception->getMessage());
        }
    }


 ?>