




<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="css/style.css" >
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">

    <title>Orient films</title>
  </head>
  <body>
    <div >
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="nav-contact">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                    aria-label="sToggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <h3>Orient Films</h3>
                        </a>
                
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-md-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Home
                        </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#!"><i class="fa fa-film" aria-hidden="true"></i>Movies
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="gallery.php"><i class="fa fa-picture-o" aria-hidden="true"></i>Gallery
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="contact.php"><i class="fa fa-phone-square" aria-hidden="true"></i>Contact
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#!"><i class="fa fa-question-circle" aria-hidden="true"></i>About-Us
                                </a>
                    </li>
                    
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2 form-control-sm" type="text" placeholder="Search" >
                        <button class="btn btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </nav>
                  
        
        
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="background-color: white; border-radius: 0%">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Profile</li>
                </ol>
            </nav>

        <div class="main" >
            <div id="bg-img">
                <container>
                    <div class="row">
                        <div class="col-4" style="height: 90vh; background-color: rgba(0,0,0, 0.5); margin: 2px 2px;">
                            <a class = "btn btn-secondary btn-lg btn-block " href = "dashboard.php"> Dashboard</a><br>
                        </div>

                            <div class="row" style = "margin: 10px 10px">
                            <form method = "POST" action = "editProfile.php">
                            <div class="form-group">
                            <label for="formGroupExampleInput">Name:</label>
                            <br /> <input type="text" name = "first_name" class="form-control" id="formGroupExampleInput" placeholder="user name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address:</label><br>
                                <input type="email" name = "name"class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">address:</label>
                                <textarea name = "user_address" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                            </div>
                        <div>

                </container>
             </div>                
        </div>

       
    </div>
        <footer>
                <div  style="text-align: center;  background-color: rgb(10, 10, 10); 
                 height: 200px; padding-top: 50px;" >
                 <div class = "footerLink">
                    <a href="#">Home</a>    |
                    <a href="#">contact</a> |
                    <a href="#">about</a>   |
                    <a href="#">testimonies</a>
                </div>
                <div>
                    <p>visit our social media handle</p>
                    <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                    <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                    <a href="#"><i class="fa fa-medium fa-lg"></i></a>
                    <p style="margin-bottom: 0; ">Designed and developed by Okemmadu eric</p>
        
                </div>
            </div>
                
            </footer>
    
        <script src="js/jquery.js" ></script>
        <script src="js/bootstrap-flash-alert.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js"></script>

        
      </body>
      
    </html>