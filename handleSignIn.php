<?php
  //START SESSION
session_start();


 //CALL-IN THE DATABASE CONNECTION

if (isset($_POST['email']) && isset($_POST['user_password'])){
    require 'db.php';
    try{
        //SELECT DATA
        $query = "SELECT first_name FROM users WHERE email=:email AND user_password=:user_password";
        $stmt = $conn->prepare($query);

        //BIND PARAMETERS
        $email = $_POST['email'];
        $user_password = hash("sha256",$_POST['user_password']);

        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':user_password', $user_password);
        $stmt->execute();

        //GETS THE NUMBER OF ROWS RETURNED
        $num = $stmt->rowCount();

        if($num<=0){
          echo "Please check your email and password very well " . "<a href='signin.php'>Go back...</a>";

        }
        
        if($num>0){
            $row = $stmt->fetch();
            $_SESSION['first_name'] = $row['first_name'];
          
            header("Location:index.php");
            exit();
        }

    }
    //SHOW ERROE
    catch(PDOException $exception)
    {
    die("Error : " . $exception->getMessage());
    }
}
else{
session_destroy();
}
/* 

session_start();

if(!empty($_SESSION['email'])) {
header('location:index.php');
}
require 'db.php';


if(isset($_POST['submit'])) {

$email = $_POST['email'];
$user_password= $_POST['user_password'];

if(empty($user) || empty($pass)) {
$message = 'All field are required';
} else {
$query = $conn->prepare("SELECT email, user_password FROM users WHERE 
email=? AND user_password=? ");
$query->execute(array($email,$user_password));
$row = $query->fetch(PDO::FETCH_BOTH);

if($query->rowCount() > 0) {
  $_SESSION['email'] = $email;
  header('location:index.php');
} else {
  $message = "Username/Password is wrong";
}


}

}
 */

?> 