<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="css/style.css" >
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <title>Orient films</title>
  </head>
  <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <h3>Orient Films</h3>
                        </a>
                
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-md-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Home
                        </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#!"><i class="fa fa-film" aria-hidden="true"></i>Movies
                                </a>
                    </li>
                    <li class="nav-item">
                            <a href = "gallery.php" class="nav-link"><i class="fa fa-picture-o" aria-hidden="true"></i>Gallery
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="contact.php"><i class="fa fa-phone-square" aria-hidden="true"></i>Contact
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#!"><i class="fa fa-question-circle" aria-hidden="true"></i>About-Us
                                </a>
                    </li>

                    <?php
                        session_start();

                        if(isset($_SESSION['first_name'])) {
                        echo "<div><h6>Welcome ".$_SESSION['first_name']."</h6></div><br>";
                            echo "<a href='logout.php' class= 'btn btn-sm'>Logout </a>";
                            
                        } 
                        
                        ?>
                    
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2 form-control-sm" type="text" placeholder="Search" >
                        <button class="btn btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </nav>
                  
        
        <div class="main" >
            
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        
                        <div class="carousel-inner" style="height: 90%;">
                          <div class="carousel-item "  >
                            <img class="d-block w-100" src="https://i.pinimg.com/originals/6e/8e/56/6e8e56de52eb67f587a40a1f4a0e34fa.gif" alt="group-films">
                            
                        </div>
                        
                          <div class="carousel-item active" >
                            <img class="d-block w-100" src="https://nypdecider.files.wordpress.com/2017/03/5-star-netflix.jpg?quality=90&strip=all&w=646&h=431&crop=1" alt="group-films">
                            
                        </div>
                          <div class="carousel-item" >
                            <img class="d-block w-100" src="https://studybreaks.com/wp-content/uploads/2018/02/tag-e1519690853279.jpg" alt="group-films">
                            
                          </div>
                        </div>
                        <div class="overlay">
                        <div class="overlay-text text-center">
                                   
                                <div class="carousel-caption d-block">
                                        <h5 >Our mission is to satisfy you customers and to help you enjoy your weekend with trending Movies </h5>
                                        <p>Explore our <a href="gallery.php">Gallery</a> to see more movies</p>
                                 <div id="getstarted" style = "margin-top: 100px;">   
                                    <a href="signup.php"  class="getstarted-smd" >Get Started</a>
                                        <?php
                                        session_start();
                                        if(!isset($_SESSION['first_name'])) {
                                        echo "<a href='signup.php' class='explore-btn explore-btn-white' >SIGN UP</a>
                                        <a href='signin.php' class='explore-btn explore-btn-white' >SIGN IN</a>";
                                        }else{
                                            echo "<a href='dashboard.php' class='explore-btn explore-btn-white' >GO TO DASHBOARD</a>";
                                        }
                                        ?>
                                </div>
                                 
                            </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                        
                      </div>
        </div>
      <div class="container-fluid" style="margin-top: 0px; background-color: #3a5a69" id="gallery">
            <h1 style="text-align: center;"> Trending movies</h1>
  
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                        <img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRkIT4mW3UlHSDKjRtOKWOWyAEngS5D_SewWFvCh48NLIAxky6KYQ" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">Kim Possible</h4>
                            <p class="card-text">This is a cartoon movie you will not regret watching, its interesting and educating</p>
                            <a href="#!" class="btn btn-info btn-md">Read more...</a>
                        </div>
                </div>
                          
            </div>
            <div class="col-md-4">
                    <div class="card">
                            <img class="card-img-top" src="https://is5-ssl.mzstatic.com/image/thumb/Music111/v4/a7/6a/88/a76a889d-949f-f181-91dc-a4a7dc7aaa38/source/400x400-75.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Agents of S.H.E.L.D</h4>
                                <p class="card-text">Agents of sheild is an educating film but full of magic, try watch it. Not too good for kids</p>
                                <a href="#!" class="btn btn-info btn-md">Read more...</a>
                            </div>
                    </div>
                              
                </div>
                <div class="col-md-4">
                        <div class="card">
                                <img class="card-img-top" src="https://i2.wp.com/admin.thenet.ng/wp-content/uploads/2017/10/movie2-400x400.jpg?resize=400%2C400" alt="Card image cap">
                                <div class="card-body">
                                    <h4 class="card-title">Pinging Saloon</h4>
                                    <p class="card-text">Nigerian movie that won the best award in saloon movies. its full of stories and comedy</p>
                                    <a href="#!" class="btn btn-info btn-md">Read more...</a>
                                </div>
                        </div>
                                  
                    </div>
            </div> 
      </div>      

    <footer>
        <div  style="text-align: center;  background-color: rgb(10, 10, 10); 
         height: 200px; padding-top: 50px;" >
         <div class = "footerLink">
            <a href="#">Home</a>    |
            <a href="#">contact</a> |
            <a href="#">about</a>   |
            <a href="#">testimonies</a>
        </div>
        <div>
            <p>visit our social media handle</p>
            <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
            <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
            <a href="#"><i class="fa fa-medium fa-lg"></i></a>
            <p style="margin-bottom: 0; ">Designed and developed by Okemmadu eric</p>

        </div>
    </div>
        
    </footer>
    
    

          <script src="js/jquery.js" ></script>
          <script src="js/bootstrap-flash-alert.min.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
          <script src="js/bootstrap.min.js"></script>
          <script src="js/js.js"></script>
  </body>
</html>
