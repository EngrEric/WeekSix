<?php


class ContactUs
{
        
    public static function process($param){
        date_default_timezone_set("Africa/Lagos");

        //OPEN OR CREATE contact.csv IF NOT EXISTING
        $file  = fopen("contact.csv","a");

        //CREATE CSV COLUMNS (RUN THIS ONCE)
        fputcsv($file, array('FirstName','LastName','Email', 'Message', 'Date'));

        //APPEND CONTACT DETAILS TO CSV FILE
        fputcsv($file, array($param['firstname'],$param['lastname'],$param['email'],$param['message'],date("Y-m-d h:i:sa")));

        //CLOSE FILE STREAM
        fclose($file);
   
        $message = "Hello ". $param['firstname'] . ", \n\nThank you for contacting us. \n\nCheers.";
        $headers = "Reply-To: Orient Admin <okemmadueric@gmail.com>\r\n";
        $headers = "Return-Path: Orient Admin <okemmadueric@gmail.com>\r\n";
        $headers = "From: Orient Admin <okemmadueric@gmail.com>\r\n";

         //SEND THE MAIL
         mail($param['email'], "Orient Film subscription", $message, $headers);
        
         /*  
         |----------------------------------------------
         |READ INFORMATION FROM CSV FILE
         |----------------------------------------------
         */
        $csv = array_map('str_getcsv', file('contact.csv'));


        echo $csv[sizeof($csv)-1][0] . '<br>';
        echo $csv[sizeof($csv)-1][1] . '<br>';
        echo $csv[sizeof($csv)-1][2] . '<br>';
        echo $csv[sizeof($csv)-1][4] . '<br>';


    }
}
?>
