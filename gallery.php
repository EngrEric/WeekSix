<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="css/style.css" >
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <title>Orient films</title>
  </head>
  <body>
    <div >
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <h3>Orient Films</h3>
                        </a>
                
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-md-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Home
                        </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#!"><i class="fa fa-film" aria-hidden="true"></i>Movies
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="gallery.php"><i class="fa fa-picture-o" aria-hidden="true"></i>Gallery
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="contact.php"><i class="fa fa-phone-square" aria-hidden="true"></i>Contact
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#!"><i class="fa fa-question-circle" aria-hidden="true"></i>About-Us
                                </a>
                    </li>
                    
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2 form-control-sm" type="text" placeholder="Search" >
                        <button class="btn btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </nav>
                  
        
        <div class="main" >
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="background-color: white; border-radius: 0%">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gallery</li>
                </ol>
            </nav>
                
        </div>
    </div>

    <div id="design" >
        <div class="design1">
        <h3 >Live a brave life when you watch brave films</h3>
        <p class="p1">Orient Films</p>
        <p >Home for movies</p>
    </div>
    
    </div>
      <div class="container-fluid" style="margin-top: 0px; background-color: #3a5a69" id="gallery">
            <h1 style="text-align: center;"><i class="fa fa-film fa-1x" aria-hidden="true"></i> Our Gallery</h1>
  
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                        <img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRkIT4mW3UlHSDKjRtOKWOWyAEngS5D_SewWFvCh48NLIAxky6KYQ" alt="Card image cap">
                        
                </div>
                          
            </div>
            <div class="col-md-4">
                    <div class="card">
                            <img class="card-img-top" src="https://is5-ssl.mzstatic.com/image/thumb/Music111/v4/a7/6a/88/a76a889d-949f-f181-91dc-a4a7dc7aaa38/source/400x400-75.jpg" alt="Card image cap">
                            
                    </div>
                              
                </div>
                <div class="col-md-4">
                        <div class="card">
                                <img class="card-img-top" src="https://i2.wp.com/admin.thenet.ng/wp-content/uploads/2017/10/movie2-400x400.jpg?resize=400%2C400" alt="Card image cap">
                               
                        </div>
                                  
                    </div>
        </div>
        <div class="row">
                <div class="col-md-4">
                    <div class="card">
                            <img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS-xX9WaLvhXqi7fWloYmnI4g3ivD7mG6pca2Onn1uGfZ1EpbRE4Q" alt="Card image cap">
                            
                    </div>
                              
                </div>
                <div class="col-md-4">
                        <div class="card">
                                <img class="card-img-top" src="https://www.thoughtco.com/thmb/DClMem-7U9PA5xSHe_mqVbHASBQ=/400x400/filters:no_upscale():max_bytes(150000):strip_icc()/jennifers-body-569528bd5f9b58eba499d268.jpg" alt="Card image cap">
                                
                        </div>
                                  
                    </div>
                    <div class="col-md-4">
                            <div class="card">
                                    <img class="card-img-top" src="https://media.vogue.in/wp-content/uploads/2018/04/The-eyes-have-it-1-400x400.jpg" alt="Card image cap">
                                   
                            </div>
                                      
                        </div>
            </div>
    </div>

    <footer>
        <div  style="text-align: center;  background-color: rgb(10, 10, 10); 
         height: 200px; padding-top: 50px;" >
         <div class = "footerLink">
            <a href="#">Home</a>    |
            <a href="#">contact</a> |
            <a href="#">about</a>   |
            <a href="#">testimonies</a>
        </div>
        <div>
            <p>visit our social media handle</p>
            <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
            <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
            <a href="#"><i class="fa fa-medium fa-lg"></i></a>
            <p style="margin-bottom: 0; ">Designed and developed by Okemmadu eric</p>

        </div>
    </div>
        
    </footer>
    </div>

    <script src="js/jquery.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>