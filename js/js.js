
$("document").ready(function(){
    $("#submit").on("click", formValidate);
    
    $(".star").hide(); 

})
// function to validate form
function formValidate (evt){

evt.preventDefault();

const firstname = $("#firstname").val();

const lastname = $("#lastname").val();

const email = $("#email").val();

// check if the email format is correct
const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const CheckEmailValue = re.test(email);

// check if any input field is empty
if(firstname === "" || firstname === "" || email === ""){

    $(".msg_error").text("fill this part with asterics");

    $(".star").css("color", "red").show();
    
    } else if(!CheckEmailValue){
        $("#email").css("border", "1px solid red");
        $ .alert("Proper email is required", "Incorrect email format");

    } else {
        $("#email").css("border", "none");
        $("#msg").text("");

        $(".star").hide();

       $ .alert("We will reach you shortly, Please check your mail: ", 
    {withTime: true,type: 'success',
    title:'Thanks for contacting us',
    icon:'glyphicon glyphicon-heart',
    minTop: 300});
    }
};

