<?php
 //START SESSION
session_start();

 //CALL-IN THE DATABASE CONNECTION
include 'db.php';

if ( isset($_POST['first_name'])  && isset($_POST['last_name']) && 
    isset($_POST['user_name']) && isset($_POST['user_password']) 
      && isset($_POST['email']) &&  isset($_POST['contact_no'])){

        try{
            //INSERT QUERY FOR USER DATA
            $query = "INSERT INTO users SET first_name = :first_name, last_name = :last_name, user_name = :user_name,
                                           user_password = :user_password, email = :email, contact_no = :contact_no";
            
            //PREPARE THE QUERY FOR EXECUTION
            $stmt = $conn->prepare($query);

            //POSTED VALUES
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $user_name = $_POST['user_name'];
            $user_password = $_POST['user_password'];
            $email = $_POST['email'];
            $contact_no = $_POST['contact_no'];

            //BINDING PARAMETERS
            $stmt->bindParam(':first_name', $first_name);
            $stmt->bindParam(':last_name', $last_name);
            $stmt->bindParam(':user_name', $user_name);
            $stmt->bindParam(':user_password', hash("sha256",$user_password));
            $stmt->bindParam(':email',$email);
            $stmt->bindParam(':contact_no', $contact_no);

            //EXECUTE THE QUERY
            $result = $stmt->execute();

            if($result){
                $_SESSION['first_name'] = $first_name;
                header("Location: signin.php");
                exit();

            }else{
                echo "<div> Unable to save your information to our database</div>";
            }

        }
   //SHOW ERROE
   catch(PDOException $exception)
    {
    die("Error : " . $exception->getMessage());
    }
} 


 ?> 