<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="css/style.css" >
    <link rel="stylesheet" href="css/signup.css" >


    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">

    <title>Orient films</title>
  </head>
  <body>
    <div >
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="nav-contact">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                    aria-label="sToggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <h3>Orient Films</h3>
                        </a>
                
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-md-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Home
                        </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#!"><i class="fa fa-film" aria-hidden="true"></i>Movies
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="gallery.php"><i class="fa fa-picture-o" aria-hidden="true"></i>Gallery
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="contact.php"><i class="fa fa-phone-square" aria-hidden="true"></i>Contact
                                </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="#!"><i class="fa fa-question-circle" aria-hidden="true"></i>About-Us
                                </a>
                    </li>
                    
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2 form-control-sm" type="text" placeholder="Search" >
                        <button class="btn btn-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </nav>
             

        <div class="main" >
            <div id="bg-img">




            <div class="container form2">

                <form class="well form-horizontal" action="handleSignUp.php" method="POST"  id="contact_form">
                <fieldset>

                <!-- Form Name -->
                <legend><center><h2><b>Registration Form</b></h2></center></legend><br>

                <!-- Text input-->
                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 control-label">First Name</label>  
                    <div class="col-md inputGroupContainer">
                        <div class="input-group">
                        <input  name="first_name" placeholder="First Name" class="form-control"  type="text">
                        </div>
                    </div>
                </div>
                </div>

                <!-- Text input-->
                <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 control-label" >Last Name</label> 
                    <div class="col-md inputGroupContainer">
                        <div class="input-group">
                        <input name="last_name" placeholder="Last Name" class="form-control"  type="text">
                        </div>
                    </div>
                </div>
                </div>
                </div>

                

                <!-- Text input-->

                <div class="form-group">
                    <label class="col-md-4 control-label">Username</label>  
                    <div class="col-md-6 inputGroupContainer">
                        <div class="input-group">
                        <input  name="user_name" placeholder="Username" class="form-control"  type="text">
                        </div>
                    </div>
                </div>

                <!-- Text input-->
                <div class = "row">
                    <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 control-label" >Password</label> 
                    <div class="col-md inputGroupContainer">
                        <div class="input-group">
                        <input name="user_password" placeholder="Password" class="form-control"  type="password">
                        </div>
                    </div>
                </div>
                </div>
              
                <!-- Text input-->
                <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 control-label" >Confirm</label> 
                    <div class="col-md inputGroupContainer">
                        <div class="input-group">
                        <input name="confirm_password" placeholder="Confirm Password" class="form-control"  type="password">
                        </div>
                    </div>
                </div>
                </div>
                </div>


                <!-- Text input-->
                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 control-label">E-Mail</label>  
                    <div class="col-md inputGroupContainer">
                        <div class="input-group">
                        <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
                        </div>
                    </div>
                </div>
                </div>


                <!-- Text input-->
                <div class="col-md-6">
                <div class="form-group">
                <label class="col-md-4 control-label">Contact No.</label>  
                    <div class="col-md inputGroupContainer">
                        <div class="input-group">
                        <input name="contact_no" placeholder="+234" class="form-control" type="text">
                        </div>
                    </div>
                </div>
                </div>
                </div>

                <!-- Select Basic -->

                <!-- Success message -->
                <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>

                <!-- Button -->
                <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><br>
               <button type="submit" class="btn btn-warning" >SUBMIT <span class="glyphicon glyphicon-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
                </div>
                </div>

                </fieldset>
                </form>
                <h4>Already a member ? <a href = "signin.php">Login</a></h4>

                </div>
            </div><!-- /.container -->

             </div>                
        </div>

       
    </div>
        <footer>
                <div  style="text-align: center;  background-color: rgb(10, 10, 10); 
                 height: 200px; padding-top: 50px;" >
                 <div class = "footerLink">
                    <a href="#">Home</a>    |
                    <a href="#">contact</a> |
                    <a href="#">about</a>   |
                    <a href="#">testimonies</a>
                </div>
                <div>
                    <p>visit our social media handle</p>
                    <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                    <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                    <a href="#"><i class="fa fa-medium fa-lg"></i></a>
                    <p style="margin-bottom: 0; ">Designed and developed by Okemmadu eric</p>
        
                </div>
            </div>
                
            </footer>
    
        <script src="js/jquery.js" ></script>
        <script src="js/bootstrap-flash-alert.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js"></script>

        
      </body>
      
    </html>